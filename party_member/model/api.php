<?php

//载入日志函数
load()->func('logging');
//记录文本日志


/**
 *向服务器POST数据(JSON格式)
 */
function postCurl($url, $arrdata){
    $header = array(
        "Content-Type: application/json",
        "Cache-Control: no-cache"
    );
    $postData = json_encode ($arrdata);

    logging_run("发送数据：");
    logging_run($postData);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,FALSE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,FALSE);
    curl_setopt($ch, CURLOPT_AUTOREFERER,1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
    curl_setopt($ch, CURLOPT_NOSIGNAL, 1);//注意，毫秒超时一定要设置这个
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS,10000);//尝试连接等待的时间
    curl_setopt($ch, CURLOPT_TIMEOUT_MS, 20000);//允许执行的最长毫秒数，cURL 7.16.2中被加入。
    $info = curl_exec($ch);


    logging_run("接收数据：");
    logging_run($info);

    if(curl_errno($ch)){
        $info = array();
        $info['ret'] = 9999;
        $info['msg'] = '与服务器通信失败';
        curl_close($ch);
        return $info;
    }
    curl_close($ch);
    return json_decode($info, true);
}

function getServer(){
    return "http://39.108.157.81:3000";
}