<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2017/9/10
 * Time: 23:56
 */

require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";

if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}

$user= getUser();

$request = postCurl(getServer()."/getPartychargeByid",array(
    "id"=>$user["id"],
));
$list = $request["data"]['list'];
$current = $request["data"]['current'];
$userInfo = getUserInfo();



include $this->template('payParty');