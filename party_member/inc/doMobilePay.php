<?php
global $_GPC,$_W;
$id = $_GPC['id'];
require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";
if(!isUser()){
    $url =$this->createMobileUrl('login');
    header("location: $url");
    setJumpUrl();
    die();
}
$user= getUser();

$request = postCurl(getServer()."/CreatePayOrder",array(
    "orderid"=>$id
));
if($request['ret']!=0){
    message($request['msg']);
}else{
    $payid = $request['data']['payid'];
    $charge=  $request['data']['charge'];
    $params = array(
        'tid' => $payid,      //充值模块中的订单号，此号码用于业务模块中区分订单，交易的识别码
        'ordersn' => $payid,  //收银台中显示的订单号
        'title' => "党费缴纳",          //收银台中显示的标题
        'fee' => $charge,      //收银台中显示需要支付的金额,只能大于 0
        'user' => $_W['member']['uid'],     //付款用户, 付款的用户名(选填项)
    );

    $this->pay($params);
}

