<?php

require_once dirname(__FILE__)."/../model/user.php";
require_once dirname(__FILE__)."/../model/api.php";
global $_GPC,$_W;
$phone=$_GPC['phone'];
$password=$_GPC['password'];
$verific=$_GPC['code'];
if ($_SESSION['verific']!=$verific){
    echo json_encode(array(
        'status'=>false,
        'result'=>"图形验证码有误".$_SESSION['code']
    ),true);
    exit();
}

$openid= $_W['openid'];
$request = postCurl(getServer()."/login",array(
    "mobilephone"=>$phone,
    "password"=>$password,
    "openid"=>$openid
));
if($request["ret"]!=0){
    echo json_encode(array('status'=>false,'result'=>"账号不存在或密码错误"),true);
    die();
}else{
    setUser($request['data']);
    echo json_encode(array('status'=>true,'result'=>"登录成功"),true);
    die();
}
